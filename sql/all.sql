CREATE TABLE
IF NOT EXISTS
    ranks (
        id              VARCHAR(10)     NOT NULL
                                        PRIMARY KEY,

        title           TEXT            NOT NULL
                                        UNIQUE,
        place           INTEGER         NOT NULL
                                        UNIQUE,

        created         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP,
        updated         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP
    );

CREATE TABLE
IF NOT EXISTS
    users (
        id              VARCHAR(10)     NOT NULL
                                        PRIMARY KEY,

        title           VARCHAR(128)    NOT NULL,
        email           TEXT            NOT NULL
                                        UNIQUE,
        hash            TEXT            NOT NULL,
        salt            VARCHAR(64)     NOT NULL,

        rank            VARCHAR(10)     NOT NULL
                                        REFERENCES ranks (id),

        unrendered      TEXT            NOT NULL,
        rendered        TEXT            NOT NULL,

        created         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP,
        updated         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP
    );

CREATE TABLE
IF NOT EXISTS
    languages (
        id              VARCHAR(10)     NOT NULL
                                        PRIMARY KEY,

        title           TEXT            NOT NULL
                                        UNIQUE,

        created         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP,
        updated         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP
    );

CREATE TABLE
IF NOT EXISTS
    ratings (
        id              VARCHAR(10)     NOT NULL
                                        PRIMARY KEY,
        
        title           TEXT            NOT NULL
                                        UNIQUE,

        created         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP,
        updated         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP
    );

CREATE TABLE
IF NOT EXISTS
    states (
        id              VARCHAR(10)     NOT NULL
                                        PRIMARY KEY,

        title           TEXT            NOT NULL
                                        UNIQUE,

        created         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP,
        updated         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP
    );

CREATE TABLE
IF NOT EXISTS
    stories (
        id              VARCHAR(10)     NOT NULL
                                        PRIMARY KEY,

        author          VARCHAR(10)     NOT NULL
                                        REFERENCES users (id),
        lang            VARCHAR(10)     NOT NULL
                                        REFERENCES languages (id),
        rating          VARCHAR(10)     NOT NULL
                                        REFERENCES ratings (id),
        state           VARCHAR(10)     NOT NULL
                                        REFERENCES states (id),

        title           VARCHAR(128)    NOT NULL,
        summary         TEXT            NOT NULL,

        unrendered      VARCHAR(2048)   NOT NULL,
        rendered        VARCHAR(2048)   NOT NULL,
        word_count      INTEGER         NOT NULL,

        created         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP,
        updated         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP
    );

CREATE TABLE
IF NOT EXISTS
    chapters (
        id              VARCHAR(10)     NOT NULL
                                        PRIMARY KEY,

        place           INTEGER         NOT NULL,
        title           TEXT            NOT NULL,

        pre_raw         TEXT            NOT NULL,
        pre_rendered    TEXT            NOT NULL,

        main_raw        TEXT            NOT NULL,
        main_rendered   TEXT            NOT NULL,
        word_count      INTEGER         NOT NULL,

        post_raw        TEXT            NOT NULL,
        post_rendered   TEXT            NOT NULL,

        created         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP,
        updated         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP
    );

CREATE TABLE
IF NOT EXISTS
    tags (
        id              VARCHAR(10)     NOT NULL
                                        PRIMARY KEY,

        title           TEXT            NOT NULL,
        category        TEXT            NOT NULL,

        created         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP,
        updated         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP
    );

CREATE TABLE
IF NOT EXISTS
    awards (
        id              VARCHAR(10)     NOT NULL
                                        PRIMARY KEY,

        category        TEXT            NOT NULL,
        place           INTEGER         NOT NULL,
        title           TEXT            NOT NULL,

        story           VARCHAR(10)     NOT NULL
                                        REFERENCES stories (id),

        created         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP,
        updated         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP
    );




CREATE TABLE
IF NOT EXISTS
    story_favorites (
        story_id        VARCHAR(10)     NOT NULL
                                        REFERENCES stories (id),
        user_id         VARCHAR(10)     NOT NULL
                                        REFERENCES users (id)
    );
CREATE UNIQUE INDEX 
IF NOT EXISTS
    story_favorites_index
        ON story_favorites (story_id, user_id);

CREATE TABLE
IF NOT EXISTS
    story_follows (
        story_id        VARCHAR(10)     NOT NULL
                                        REFERENCES stories (id),
        user_id         VARCHAR(10)     NOT NULL
                                        REFERENCES users (id)
    );
CREATE UNIQUE INDEX 
IF NOT EXISTS
    story_follows_index
        ON story_follows (story_id, user_id);

CREATE TABLE
IF NOT EXISTS
    story_tags (
        story_id        VARCHAR(10)     NOT NULL
                                        REFERENCES stories (id),
        tag_id          VARCHAR(10)     NOT NULL
                                        REFERENCES tags (id)
    );
CREATE UNIQUE INDEX 
IF NOT EXISTS
    story_tags_index
        ON story_tags (story_id, tag_id);

CREATE TABLE
IF NOT EXISTS
    user_favorites (
        author_id       VARCHAR(10)     NOT NULL
                                        REFERENCES users (id),
        user_id         VARCHAR(10)     NOT NULL
                                        REFERENCES users (id)
    );
CREATE UNIQUE INDEX 
IF NOT EXISTS
    user_favorites_index
        ON user_favorites (author_id, user_id);

CREATE TABLE
IF NOT EXISTS
    user_follows (
        author_id       VARCHAR(10)     NOT NULL
                                        REFERENCES users (id),
        user_id         VARCHAR(10)     NOT NULL
                                        REFERENCES users (id)
    );
CREATE UNIQUE INDEX 
IF NOT EXISTS
    user_follows_index
        ON user_follows (author_id, user_id);




INSERT INTO ranks (
    id, title, place, created, updated
) VALUES (
    'zoV0F7CPci', 'Owner', 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
), (
    'hr8s1B7rAQ', 'Moderator', 10, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
), (
    '5q8iRMdYt7', 'User', 20, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
) ON CONFLICT DO NOTHING;

INSERT INTO ratings (
    id, title, created, updated
) VALUES (
    'gFTtJZbNPI', 'General', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
), (
    'tF0veA7ZBE', 'Teen', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
), (
    '7mOAYnZIoN', 'Mature', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
), (
    'Feic1UIUdk', 'Explicit', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
), (
    '4NdjcnUVaI', 'Not Rated', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
) ON CONFLICT DO NOTHING;

INSERT INTO states (
    id, title, created, updated
) VALUES (
    'QfmBzbyO7J', 'Abandoned', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
), (
    'Hp0Atr7727', 'Work In Progress', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
), (
    '9R72RXTp33', 'Completed', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
) ON CONFLICT DO NOTHING;
