pub mod controllers;
pub mod models;
pub mod views;

use {
    crate::common::ArchiveState,
    actix_web::{web, Error, HttpResponse, Scope},
    std::sync::Arc,
};

pub struct ArchiveWebClient;

impl ArchiveWebClient {
    pub fn configure(scope: Scope) -> Scope {
        scope
    }
}
