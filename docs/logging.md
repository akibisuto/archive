# Logging

```json
{
    "timestamp": "",
    "time_elapsed": 1,
    "request": {
        "bytes": "",
        "path": "",
        "query": "",
        "user_agent": "",
    },
    "response": {
        "bytes": "",
        "status": "",
        "text": "",
    }
}
```

```json
{
    "timestamp": "2018-04-16 04:59:59",
    "time_elapsed": 1,
    "client_ip": "139.130.87.202",
    "client_continent": "OC",
    "client_country": "Australia",
    "client_region": "NSW",
    "client_city": "sydney",
    "client_latitude": "-33.867",
    "client_longitude": "151.207",
    "client_timezone": "1000",
    "client_connection": "broadband",
    "request": "GET",
    "request_host": "rubygems.org",
    "request_path": "/versions",
    "request_query": "",
    "request_bytes": 387,
    "user_agent": "bundler/1.16.1 rubygems/2.6.11 ruby/2.4.1 (x86_64-pc-linux-gnu) command/install options/no_install,mirror.https://rubygems.org/,mirror.https://rubygems.org/.fallback_timeout/,path 59dbf8e99fa09c0a",
    "http2": false,
    "tls": true,
    "tls_version": "TLSv1.2",
    "tls_cipher": "ECDHE-RSA-AES128-GCM-SHA256",
    "response_status": "200",
    "response_text": "OK",
    "response_bytes": 1855,
    "response_cache": "HIT",
    "cache_state": "HIT-CLUSTER",
    "cache_lastuse": 1523854799.756,
    "cache_hits": 1,
    "server_region": "APAC",
    "server_datacenter": "MEL"
}
```