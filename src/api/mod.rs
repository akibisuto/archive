pub mod models;
pub mod mutations;
pub mod queries;

use {
    crate::{common::ArchiveState, CONFIG},
    actix_web::{web, Error, HttpResponse, Scope},
    futures::future::Future,
    juniper::{http::{graphiql::graphiql_source, GraphQLRequest}, RootNode},
    std::sync::Arc,
};

pub use self::{mutations::MutationRoot, queries::QueryRoot};

pub struct ArchiveWebApi;

impl ArchiveWebApi {
    pub fn configure(state: ArchiveState, scope: Scope) -> Scope {
        scope
            .data(Arc::new(create_schema(state)))
            .service(web::resource("/graphql").route(web::post().to_async(graphql)))
            .service(web::resource("/graphiql").route(web::get().to(graphiql)))
    }
}

fn graphql(
    schema: web::Data<Arc<Schema>>,
    data: web::Json<GraphQLRequest>,
) -> impl Future<Item = HttpResponse, Error = Error> {
    web::block(move || {
        let res = data.execute(&schema, &());
        Ok::<_, serde_json::error::Error>(serde_json::to_string(&res)?)
    })
    .map_err(Error::from)
    .and_then(|user| {
        Ok(HttpResponse::Ok()
            .content_type("application/json")
            .body(user))
    })
}

fn graphiql() -> HttpResponse {
    let html = graphiql_source(&format!("http://{}:{}/api/graphql", CONFIG.server.host, CONFIG.server.port));

    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(html)
}

pub type Schema = RootNode<'static, QueryRoot, MutationRoot>;

pub fn create_schema(state: ArchiveState) -> Schema {
    Schema::new(
        QueryRoot {
            state: state.clone(),
        },
        MutationRoot {
            state: state.clone(),
        },
    )
}

#[derive(juniper::GraphQLObject, serde::Deserialize, serde::Serialize)]
pub struct DummyPayload {
    pub dummy: bool,
}
