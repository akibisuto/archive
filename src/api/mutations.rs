use {
    crate::{
        api::{models::{story, tag}, DummyPayload},
        common::{ArchiveState, Error, Pool},
    },
    argonautica::{Hasher, Verifier},
    chrono::{DateTime, Utc},
    juniper::FieldResult,
};

pub struct MutationRoot {
    pub state: ArchiveState,
}

#[juniper::object(name = "Mutation")]
impl MutationRoot {
    fn activate(
        &self,
        code: String,
        email: String,
        password: String,
    ) -> FieldResult<DummyPayload> {
        Ok(DummyPayload::activate(
            self.state.pool.clone(),
            &self.state.pass_salt,
            code,
            email,
            password,
        )?)
    }

    fn login(&self, email: String, password: String) -> FieldResult<LoginPayload> {
        Ok(LoginPayload::login(
            self.state.pool.clone(),
            &self.state.pass_salt,
            &self.state.jwt_salt,
            email,
            password,
        )?)
    }

    fn register(
        &self,
        username: String,
        email: String,
        password: String,
    ) -> FieldResult<RegisterPayload> {
        Ok(RegisterPayload::register(
            self.state.pool.clone(),
            &self.state.pass_salt,
            username,
            email,
            password,
        )?)
    }

    fn add_story(
        &self,
        token: String,
        story: story::AddStoryInput,
    ) -> FieldResult<story::AddStoryPayload> {
        Ok(story::AddStoryPayload { id: String::new() })
    }

    fn add_tag(&self, token: String, story_id: String, tag_id: String) -> FieldResult<tag::AddTagPayload> {
        Ok(tag::AddTagPayload::add(self.state.pool.clone())?)
    }
}

impl DummyPayload {
    pub(crate) fn activate(
        pool: Pool,
        pass_salt: &str,
        activation_code: String,
        email: String,
        password: String,
    ) -> Result<Self, Error> {
        if email.len().gt(&128) {
            return Err(Error::argument().context("email argument is longer than 128 characters"));
        }

        if password.len().gt(&256) {
            return Err(
                Error::argument().context("password argument is longer than 256 characters")
            );
        }

        let conn = pool.get()?;

        let rows = conn.query(
            "SELECT Id, Name, Salt, Hash, ActivationCode FROM User WHERE Email = ? LIMIT 1;",
            &[&email],
        )?;

        if rows.is_empty() {
            Err(Error::incorrect())
        } else {
            let row = rows.get(0);

            let id: String = row.get("Id");
            let name: String = row.get("Name");
            let salt: String = row.get("Salt");
            let hash: String = row.get("Hash");
            let code: String = row.get("ActivationCode");

            let verified = Verifier::default()
                .with_secret_key(pass_salt)
                .with_password(format!("{}-{}", salt, password))
                .with_hash(&hash)
                .verify()?;

            if verified {
                if activation_code == code {
                    conn.execute(
                        "UPDATE User SET Activated = TRUE WHERE Id = ?1 AND Name = ?2 AND Salt = ?3 AND Hash = ?4 AND ActivationCode = ?5;",
                        &[
                            &id,
                            &name,
                            &salt,
                            &hash,
                            &code,
                        ],
                    )?;

                    Ok(DummyPayload { dummy: true })
                } else {
                    Err(Error::activated())
                }
            } else {
                Err(Error::incorrect())
            }
        }
    }
}

#[derive(juniper::GraphQLObject, serde::Deserialize, serde::Serialize)]
pub struct LoginPayload {
    pub token: String,
}

impl LoginPayload {
    pub(crate) fn login(
        pool: Pool,
        pass_salt: &str,
        jwt_salt: &str,
        email: String,
        password: String,
    ) -> Result<Self, Error> {
        if email.len().gt(&128) {
            return Err(Error::argument().context("email argument is longer than 128 characters"));
        }

        if password.len().gt(&256) {
            return Err(
                Error::argument().context("password argument is longer than 256 characters")
            );
        }

        let conn = pool.get()?;

        let rows = conn.query(
            "SELECT Id, Name, Salt, Hash, Activated FROM User WHERE Email = ? LIMIT 1;",
            &[&email],
        )?;

        if rows.is_empty() {
            Err(Error::incorrect())
        } else {
            let row = rows.get(0);

            let id: String = row.get("Id");
            let name: String = row.get("Name");
            let salt: String = row.get("Salt");
            let hash: String = row.get("Hash");
            let activated: bool = row.get("Activated");

            if activated {
                let verified = Verifier::default()
                    .with_secret_key(pass_salt)
                    .with_password(format!("{}-{}", salt, password))
                    .with_hash(hash)
                    .verify()?;

                if verified {
                    Ok(LoginPayload {
                        token: frank_jwt::encode(
                            serde_json::json!({}),
                            &jwt_salt,
                            &serde_json::json!({
                                "id": id,
                                "name": name,
                                "email": email,
                            }),
                            frank_jwt::Algorithm::HS256,
                        )?,
                    })
                } else {
                    Err(Error::incorrect())
                }
            } else {
                Err(Error::activated())
            }
        }
    }
}

#[derive(juniper::GraphQLObject, serde::Deserialize, serde::Serialize)]
pub struct RegisterPayload {
    pub email: String,
}

impl RegisterPayload {
    pub(crate) fn register(
        pool: Pool,
        pass_salt: &str,
        username: String,
        email: String,
        password: String,
    ) -> Result<Self, Error> {
        if username.len().gt(&56) {
            return Err(Error::argument().context("username argument is longer than 56 characters"));
        }

        if email.len().gt(&128) {
            return Err(Error::argument().context("email argument is longer than 128 characters"));
        }

        if password.len().gt(&256) {
            return Err(
                Error::argument().context("password argument is longer than 256 characters")
            );
        }

        let conn = pool.get()?;

        let rows = conn.query("SELECT Email FROM User WHERE Email = ? LIMIT 1;", &[&email])?;

        if !rows.is_empty() {
            Err(Error::email())
        } else {
            let salt = crate::nanoid!(64);

            let hash = Hasher::default()
                .with_secret_key(pass_salt)
                .with_password(format!("{}-{}", salt, password))
                .hash()?;

            let date_time: DateTime<Utc> = Utc::now();
            let naive = date_time.naive_utc();

            let trans = conn.transaction()?;

            let activation_code = crate::nanoid!(64);

            trans.execute(
                "INSERT INTO User (Id, Name, Email, Salt, Hash, Activated, ActivationCode, Created, Updated) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9);",
                &[
                    &crate::nanoid!(10),
                    &username,
                    &email,
                    &salt,
                    &hash,
                    &naive,
                    &naive,
                    &false,
                    &activation_code,
                ]
            )?;

            trans.commit()?;

            // TODO send email for activation

            Ok(RegisterPayload { email })
        }
    }
}
