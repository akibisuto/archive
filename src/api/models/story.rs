use {
    crate::{
        api::models::{Connection, Edge, PageInfo},
        common::{Error, Pool},
    },
    postgres::to_sql_checked,
};

#[derive(juniper::GraphQLObject, serde::Deserialize, serde::Serialize)]
pub struct Story {
    pub id: String,

    pub title: String,
    pub summary: String,

    pub rating: Rating,
    pub state: State,

    pub chapter_count: i32,
    pub word_count: i32,
}

#[juniper::object(name = "StoryConnection")]
impl Connection<Story> {
    fn edges(&self) -> &Vec<Edge<Story>> {
        &self.edges
    }

    fn pageInfo(&self) -> &PageInfo {
        &self.info
    }

    fn totalCount(&self) -> &i32 {
        &self.total
    }
}

#[juniper::object(name = "StoryEdge")]
impl Edge<Story> {
    fn cursor(&self) -> &String {
        &self.cursor
    }

    fn node(&self) -> &Story {
        &self.node
    }
}

#[derive(
    Clone,
    Copy,
    Debug,
    PartialEq,
    PartialOrd,
    juniper::GraphQLEnum,
    postgres_derive::FromSql,
    postgres_derive::ToSql,
    serde::Deserialize,
    serde::Serialize,
)]
pub enum Rating {
    #[graphql(name = "EXPLICIT")]
    Explicit = 5,
}

#[derive(
    Clone,
    Copy,
    Debug,
    PartialEq,
    PartialOrd,
    juniper::GraphQLEnum,
    postgres_derive::FromSql,
    postgres_derive::ToSql,
    serde::Deserialize,
    serde::Serialize,
)]
pub enum State {
    #[graphql(name = "COMPLETED")]
    Complete = 3,
    #[graphql(name = "WORK_IN_PROGRESS")]
    WorkInProgress = 2,
    #[graphql(name = "HIATUS")]
    Hiatus = 1,
    #[graphql(name = "ABANDONED")]
    Abandoned = 0,
}

#[derive(juniper::GraphQLInputObject, serde::Deserialize, serde::Serialize)]
pub struct AddStoryInput {
    pub title: Option<String>,
    pub summary: Option<String>,

    pub rating: Option<Rating>,
    pub state: Option<State>,

    pub chapter_count: Option<i32>,
    pub word_count: Option<i32>,
}

#[derive(juniper::GraphQLObject, serde::Deserialize, serde::Serialize)]
pub struct AddStoryPayload {
    pub id: String,
}

impl AddStoryPayload {
    pub(crate) fn add(pool: Pool) -> Result<Self, Error> {
        Err(Error::incorrect())
    }
}

#[derive(juniper::GraphQLInputObject, serde::Deserialize, serde::Serialize)]
pub struct UpdateStoryInput {
    pub title: Option<String>,
    pub summary: Option<String>,

    pub rating: Option<Rating>,
    pub state: Option<State>,

    pub chapter_count: Option<i32>,
    pub word_count: Option<i32>,
}

#[derive(juniper::GraphQLObject, serde::Deserialize, serde::Serialize)]
pub struct UpdateStoryPayload {
    pub id: String,
}

impl UpdateStoryPayload {
    pub(crate) fn update(pool: Pool) -> Result<Self, Error> {
        Err(Error::incorrect())
    }
}

#[derive(juniper::GraphQLObject, serde::Deserialize, serde::Serialize)]
pub struct RemoveStoryPayload {
    pub id: String,
}

impl RemoveStoryPayload {
    pub(crate) fn delete(pool: Pool) -> Result<Self, Error> {
        Err(Error::incorrect())
    }
}
