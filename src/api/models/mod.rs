pub mod chapter;
pub mod review;
pub mod story;
pub mod tag;
pub mod user;

#[derive(juniper::GraphQLObject, serde::Deserialize, serde::Serialize)]
pub struct AuthPayload {
    pub token: String,
}

#[derive(juniper::GraphQLObject, serde::Deserialize, serde::Serialize)]
pub struct Message {
    pub messages: Vec<String>,
}

#[derive(juniper::GraphQLObject, serde::Deserialize, serde::Serialize)]
pub struct PageInfo {
    #[graphql(name = "hasNextPage")]
    pub next: bool,
    #[graphql(name = "hasPreviousPage")]
    pub prev: bool,
    #[graphql(name = "startCursor")]
    pub start: Option<String>,
    #[graphql(name = "endCursor")]
    pub end: Option<String>,
}

pub struct Connection<N> {
    pub edges: Vec<Edge<N>>,
    pub info: PageInfo,
    pub total: i32,
}

pub struct Edge<N> {
    pub cursor: String,
    pub node: N,
}
