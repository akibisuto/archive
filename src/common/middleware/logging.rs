use {
    crate::common::Error,
    actix_http::http::{header::USER_AGENT, HeaderValue, Method, Version},
    actix_web::dev::{Service, ServiceRequest, ServiceResponse, Transform},
    futures::{
        future::{ok, FutureResult},
        Future, Poll,
    },
    slog::Logger,
    std::{collections::HashSet, rc::Rc /* time::Instant */},
};

pub struct Logging(Rc<Inner>);

struct Inner {
    log: Logger,
    exclude: HashSet<String>,
}

impl Logging {
    pub fn new(log: Logger) -> Logging {
        Logging(Rc::new(Inner {
            log,
            exclude: HashSet::new(),
        }))
    }
}

impl<S, B> Transform<S> for Logging
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type InitError = ();
    type Transform = LoggingMiddleware<S>;
    type Future = FutureResult<Self::Transform, Self::InitError>;

    fn new_transform(&self, service: S) -> Self::Future {
        ok(LoggingMiddleware {
            service,
            inner: self.0.clone(),
        })
    }
}

pub struct LoggingMiddleware<S> {
    service: S,
    inner: Rc<Inner>,
}

impl<S, B> Service for LoggingMiddleware<S>
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Future = Box<Future<Item = Self::Response, Error = Self::Error>>;

    fn poll_ready(&mut self) -> Poll<(), Self::Error> {
        self.service.poll_ready()
    }

    fn call(&mut self, req: ServiceRequest) -> Self::Future {
        let inner = self.inner.clone();
        let info = req.connection_info();
        let address = info.remote().unwrap_or("UNKNOWN");
        let bytes = req.query_string().len();
        let method = match req.method() {
            &Method::GET => "GET",
            &Method::POST => "POST",
            &Method::PUT => "PUT",
            &Method::DELETE => "DELETE",
            &Method::HEAD => "HEAD",
            &Method::OPTIONS => "OPTIONS",
            &Method::CONNECT => "CONNECT",
            &Method::PATCH => "PATCH",
            &Method::TRACE => "TRACE",
            _ => unreachable!(),
        };
        let path = req.path().to_string();
        let query = req.query_string().to_string();
        let user_agent = req
            .headers()
            .get(USER_AGENT)
            .unwrap_or(&HeaderValue::from_static("UNKNOWN"))
            .to_str()
            .unwrap_or("UNKNOWN")
            .to_string();
        let version = match req.version() {
            Version::HTTP_09 => "0.9",
            Version::HTTP_10 => "1.0",
            Version::HTTP_11 => "1.1",
            Version::HTTP_2 => "2.0",
        };

        Box::new(self.service.call(req).map(move |res| {
            slog::info!(
                inner.log,
                "";
                "type" => "Response",
                "request_address" => address,
                "request_bytes" => bytes,
                "request_method" => method,
                "request_path" => path,
                "request_query" => query,
                "request_user_agent" => user_agent,
                "request_version" => version,
                // TODO: somehow get response size
                // "response_bytes" => match res.response().body() {
                //     ResponseBody::Body(ref body) => body.size(),
                //     ResponseBody::Other(ref body) => body.size(),
                // },
                "response_status" => res.status().as_str().to_string(),
                "response_text" => res.status().canonical_reason().unwrap_or("UNKNOWN").to_string(),
                // TODO: time elapsed, only example is built-in actix logger
                // "time_elapsed" => if let Some(then) = req.extensions().get::<Instant>() {
                //     let now =  now.duration_since(*then);
                //
                //     now.as_secs() as f64 + f64::from(now.subsec_nanos()) * 1e-9
                // } else {
                //     0.0
                // }
            );

            res
        }))
    }
}
