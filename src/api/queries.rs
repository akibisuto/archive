use {
    crate::{api::models::story, common::ArchiveState},
    juniper::FieldResult,
};

pub struct QueryRoot {
    pub state: ArchiveState,
}

#[juniper::object(name = "Query")]
impl QueryRoot {
    pub fn story(story_id: String) -> FieldResult<story::Story> {
        Ok(story::Story {
            id: story_id,
            title: String::new(),
            summary: String::new(),
            rating: story::Rating::Explicit,
            state: story::State::Complete,
            chapter_count: 1,
            word_count: 128,
        })
    }
}
