// https://gist.github.com/tomaka/67b54450a2ea11cf2f894d3fedcf7247
#[macro_use]
macro_rules! database {
    (query($conn:ident) => $query:expr, ([$($param:expr),*], {$($out_field:ident: $ty:ty),+ })) => ({
        #[derive(Debug, Clone)]
        #[allow(non_snake_case)]
        struct Output {
            $(
                $out_field: $ty,
            )+
        }

        let stmt = $db.prepare_cached($query).unwrap();
        let rows = stmt.query(&[$($param),*]).unwrap();
        rows.into_iter()
            .map(|row| {
                Output {
                    $(
                        $out_field: match row.get_opt(stmt
                            .columns()
                            .iter()
                            .position(|c| c.name() == stringify!($out_field))
                            .expect(&format!("no field named {}", stringify!($out_field)))
                        ) {
                            Some(Ok(f)) => f,
                            Some(Err(err)) => panic!("Error while executing {:?}: {:?}", $query, err),
                            None => panic!("Index doesn't reference a column"),
                        }
                    ),+
                }
            })
        .collect::<Vec<_>>()
    });
}
