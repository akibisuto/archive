#![recursion_limit = "256"]

use {
    crate::{api::ArchiveWebApi, client::ArchiveWebClient, common::ArchiveState},
    actix_rt::System,
    actix_web::{web, App, HttpServer},
    slog::{Drain, Logger},
    std::io,
};

const GIT_VERSION : &str = /* git_version::git_version!() */ "TODO";

mod api;
mod client;
mod common;

use crate::common::config::Config;

lazy_static::lazy_static! {
    pub static ref CONFIG: Config = Config::load();
}

fn main() -> io::Result<()> {
    human_panic::setup_panic!();

    let address = format!("{}:{}", &CONFIG.server.host, &CONFIG.server.port);

    let drain = slog_json::Json::new(std::io::stdout())
        .add_default_keys()
        .build()
        .fuse();
    let drain = slog_async::Async::new(drain).build().fuse();

    let root = Logger::root(
        drain.fuse(),
        slog::o!("version" => env!("CARGO_PKG_VERSION"), "build-id" => GIT_VERSION),
    );

    let sys = System::new("Archive");

    let state = ArchiveState::new(root.clone(), "");

    HttpServer::new(move || {
        App::new()
            .data(web::Data::new(state.clone()))
            .service(ArchiveWebApi::configure(
                state.clone(),
                web::scope("/api"),
            ))
            .service(ArchiveWebClient::configure(web::scope("/")))
    })
    .bind(address)?
    .start();

    slog::info!(root, "Stating Archive"; "type" => "StartUp", "host" => &CONFIG.server.host, "port" => &CONFIG.server.port);
    sys.run()
}
