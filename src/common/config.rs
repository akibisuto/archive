use std::fs::File;
use std::io::prelude::*;

use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Config {
    pub database: Database,
    pub server: Server,
}

impl Config {
    pub(crate) fn load() -> Config {
        let mut file = File::open("config.yml").expect("unable to open config file");
        let mut buffer = String::new();

        let _ = file
            .read_to_string(&mut buffer)
            .expect("unable to read file contents");

        serde_yaml::from_str(&buffer).expect("config file is not valid yaml")
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Database {
    pub salt: String,
    pub uri: String,
    pub tls: Option<DatabaseTls>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct DatabaseTls {
    pub cert: String,
    pub key: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Server {
    pub host: String,
    pub port: String,
}
