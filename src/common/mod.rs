pub mod config;
pub mod error;
pub mod middleware;
#[macro_use]
pub mod nanoid;

pub use self::{config::Config, error::Error};

use {
    postgres::params::IntoConnectParams,
    r2d2_postgres::{PostgresConnectionManager, TlsMode},
    slog::Logger,
    std::{env, sync::Arc},
};

pub type Pool = r2d2::Pool<PostgresConnectionManager>;

#[derive(Clone)]
pub struct ArchiveState {
    pub log: Logger,
    pub pool: Pool,
    pub jwt_salt: Arc<String>,
    pub pass_salt: Arc<String>,
}

impl ArchiveState {
    pub fn new(log: Logger, params: impl IntoConnectParams) -> Self {
        let manager = PostgresConnectionManager::new(params, TlsMode::None)
            .expect("Unable connect to PostgreSQL");

        let pool = r2d2::Pool::new(manager).expect("Unable to fill connection pool");

        {
            let conn = pool.get().expect("unable to get connection from pool");

            conn.batch_execute(include_str!("../schema.pgsql"))
                .expect("unable to execute schema");
        }

        Self {
            jwt_salt: Arc::new(env::var("ARCHIVE_JWT_SALT").unwrap_or_else(|_| {
                slog::warn!(log.clone(), "The environmental variable `ARCHIVE_JWT_SALT` is missing, you must fix this as soon as possible");

                String::new()
            })),
            pass_salt: Arc::new(env::var("ARCHIVE_PASS_SALT").unwrap_or_else(|_| {
                slog::warn!(log.clone(), "The environmental variable `ARCHIVE_PASS_SALT` is missing, you must fix this as soon as possible");

                String::new()
            })),
            log,
            pool,
        }
    }
}
