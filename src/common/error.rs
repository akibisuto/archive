use {
    juniper::{FieldError, IntoFieldError, Value},
    std::{borrow::Cow, error::Error as StdError, fmt, io},
};

#[derive(Debug)]
pub enum Error {
    Internal(InternalError),
    External(ExternalError),
}

impl Error {
    pub fn activated() -> Self {
        Error::Internal(InternalError {
            context: None,
            kind: ErrorKind::NotActivated,
        })
    }

    pub fn argument() -> Self {
        Error::Internal(InternalError {
            context: None,
            kind: ErrorKind::Argument,
        })
    }

    pub fn email() -> Self {
        Error::Internal(InternalError {
            context: None,
            kind: ErrorKind::EmailInUse,
        })
    }

    pub fn incorrect() -> Self {
        Error::Internal(InternalError {
            context: None,
            kind: ErrorKind::Incorrect,
        })
    }

    pub fn context(mut self, ctx: impl Into<Cow<'static, str>>) -> Self {
        match &mut self {
            Error::Internal(ref mut err) => {
                err.context = Some(ctx.into());
            }
            Error::External(_) => {}
        }

        self
    }
}

impl StdError for Error {
    fn description(&self) -> &str {
        match self {
            Error::Internal(internal) => internal.kind.as_str(),
            Error::External(external) => external.description(),
        }
    }

    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        match self {
            Error::External(external) => external.source(),
            Error::Internal(_) => None,
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Internal(internal) => internal.fmt(fmt),
            Error::External(external) => external.fmt(fmt),
        }
    }
}

impl IntoFieldError for Error {
    fn into_field_error(self) -> FieldError {
        FieldError::new(self, Value::Null)
    }
}

#[derive(Debug)]
pub struct InternalError {
    context: Option<Cow<'static, str>>,
    kind: ErrorKind,
}

impl fmt::Display for InternalError {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(ctx) = &self.context {
            write!(fmt, "[internal] {}: {}", self.kind.as_str(), ctx)
        } else {
            write!(fmt, "[internal] {}", self.kind.as_str())
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum ErrorKind {
    Argument,
    EmailInUse,
    Incorrect,
    NotActivated,
}

impl ErrorKind {
    pub fn as_str(self) -> &'static str {
        match self {
            ErrorKind::Argument => "argument not valid",
            ErrorKind::EmailInUse => "email is already in use",
            ErrorKind::Incorrect => "email or password is incorrect",
            ErrorKind::NotActivated => "account is not activated",
        }
    }
}

#[derive(Debug)]
pub enum ExternalError {
    Argon(argonautica::Error),
    Io(io::Error),
    Jwt(frank_jwt::Error),
    Postgres(postgres::Error),
    R2d2(r2d2::Error),
}

impl fmt::Display for ExternalError {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ExternalError::Argon(err) => err.fmt(fmt),
            ExternalError::Io(err) => err.fmt(fmt),
            ExternalError::Jwt(err) => err.fmt(fmt),
            ExternalError::Postgres(err) => err.fmt(fmt),
            ExternalError::R2d2(err) => err.fmt(fmt),
        }
    }
}

impl StdError for ExternalError {
    fn description(&self) -> &str {
        match self {
            ExternalError::Argon(_) => "argon error",
            ExternalError::Io(err) => err.description(),
            ExternalError::Jwt(err) => err.description(),
            ExternalError::Postgres(err) => err.description(),
            ExternalError::R2d2(err) => err.description(),
        }
    }

    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        match self {
            ExternalError::Argon(_) => None,
            ExternalError::Io(err) => err.source(),
            ExternalError::Jwt(err) => err.source(),
            ExternalError::Postgres(err) => err.source(),
            ExternalError::R2d2(err) => err.source(),
        }
    }
}

impl From<argonautica::Error> for Error {
    fn from(err: argonautica::Error) -> Error {
        Error::External(ExternalError::Argon(err))
    }
}

impl From<frank_jwt::Error> for Error {
    fn from(err: frank_jwt::Error) -> Error {
        Error::External(ExternalError::Jwt(err))
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error::External(ExternalError::Io(err))
    }
}

impl From<postgres::Error> for Error {
    fn from(err: postgres::Error) -> Error {
        Error::External(ExternalError::Postgres(err))
    }
}

impl From<r2d2::Error> for Error {
    fn from(err: r2d2::Error) -> Error {
        Error::External(ExternalError::R2d2(err))
    }
}
