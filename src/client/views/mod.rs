pub mod help;
pub mod privacy;
pub mod tos;

pub use self::{help::HelpView, privacy::PrivacyView, tos::TosView};
