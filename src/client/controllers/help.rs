use actix_web::{web, Responder};

use crate::{client::views::HelpView, common::ArchiveState};

impl HelpView {
    pub fn get(db: web::Data<ArchiveState>) -> impl Responder {
        "Welcome!"
    }
}
