use postgres::to_sql_checked;

#[derive(
    Debug, PartialEq, PartialOrd, juniper::GraphQLObject, serde::Deserialize, serde::Serialize,
)]
pub struct User {
    pub id: String,

    pub name: String,
    pub email: Option<String>,
    pub role: Role,

    pub profile: UserProfile,
}

#[derive(
    Clone,
    Copy,
    Debug,
    PartialEq,
    PartialOrd,
    juniper::GraphQLEnum,
    postgres_derive::FromSql,
    postgres_derive::ToSql,
    serde::Deserialize,
    serde::Serialize,
)]
pub enum Role {
    #[graphql(name = "ADMIN")]
    Admin = 3,
    #[graphql(name = "MODERATOR")]
    Moderator = 2,
    #[graphql(name = "MEMBER")]
    Member = 1,
}

#[derive(
    Debug, PartialEq, PartialOrd, juniper::GraphQLObject, serde::Deserialize, serde::Serialize,
)]
pub struct UserProfile {}
