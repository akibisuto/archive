use {
    crate::{api::models::{Connection, Edge, PageInfo}, common::{Error, Pool}},
    postgres::to_sql_checked,
};

#[derive(juniper::GraphQLObject, serde::Deserialize, serde::Serialize)]
pub struct Tag {
    pub id: String,

    pub name: String,

    pub kind: TagKind,
}

#[juniper::object(name = "TagConnection")]
impl Connection<Tag> {
    fn edges(&self) -> &Vec<Edge<Tag>> {
        &self.edges
    }

    fn pageInfo(&self) -> &PageInfo {
        &self.info
    }

    fn totalCount(&self) -> &i32 {
        &self.total
    }
}

#[juniper::object(name = "TagEdge")]
impl Edge<Tag> {
    fn cursor(&self) -> &String {
        &self.cursor
    }

    fn node(&self) -> &Tag {
        &self.node
    }
}

#[derive(
    Clone,
    Copy,
    Debug,
    PartialEq,
    PartialOrd,
    juniper::GraphQLEnum,
    postgres_derive::FromSql,
    postgres_derive::ToSql,
    serde::Deserialize,
    serde::Serialize,
)]
pub enum TagKind {
    #[graphql(name = "WARNING")]
    Warning = 4,
    #[graphql(name = "PAIRING")]
    Pairing = 3,
    #[graphql(name = "CHARACTER")]
    Character = 2,
    #[graphql(name = "GENERAL")]
    General = 1,
}

#[derive(juniper::GraphQLObject, serde::Deserialize, serde::Serialize)]
pub struct AddTagPayload {}

impl AddTagPayload {
    pub(crate) fn add(
        pool: Pool,
        token: &str,
        story_id: &str,
        tag_id: &str
    ) -> Result<Self, Error> {
        Ok(Self {})
    }
}

#[derive(juniper::GraphQLObject, serde::Deserialize, serde::Serialize)]
pub struct RemoveTagPayload {}