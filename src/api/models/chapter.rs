pub struct Chapter {
    pub id: String,

    pub title: String,

    pub pre_text: String,
    pub main_text: String,
    pub post_text: String,

    pub word_count: i32,
}
