CREATE TABLE
IF NOT EXISTS
    Language (
        Id              VARCHAR(10)     PRIMARY KEY,

        Name            VARCHAR(56)     NOT NULL,

        Created         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP,
        Updated         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP
    );

CREATE TYPE 
IF NOT EXISTS
    Rating AS ENUM ();

CREATE TYPE 
IF NOT EXISTS
    Role AS ENUM ("Admin", "Moderator", "Member");

CREATE TYPE 
IF NOT EXISTS
    State AS ENUM ("Completed", "WorkInProgress", "Abandoned", "Hiatus");


CREATE TABLE
IF NOT EXISTS
    User (
        Id              VARCHAR(10)     PRIMARY KEY,

        Name            VARCHAR(56)     NOT NULL,
        Email           VARCHAR(128)    NOT NULL
                                        UNIQUE,
        Salt            VARCHAR(128)    NOT NULL,
        Hash            VARCHAR(128)    NOT NULL,

        Role            Role            NOT NULL,

        Activated       BOOLEAN         NOT NULL,
        ActivationCode  VARCHAR(64)     NOT NULL
                                        UNIQUE,

        Created         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP,
        Updated         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP
    );

CREATE TABLE
IF NOT EXISTS
    Story (
        Id              VARCHAR(10)     PRIMARY KEY,

        Title           VARCHAR(56)     NOT NULL,
        Summary         VARCHAR(512)    NOT NULL,

        Language        VARCHAR(10)     NOT NULL
                                        REFERENCES Language (Id),
        Rating          VARCHAR(10)     NOT NULL
                                        REFERENCES Rating (Id),
        State           VARCHAR(10)     NOT NULL
                                        REFERENCES State (Id),

        Created         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP,
        Updated         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP
    );

CREATE TABLE
IF NOT EXISTS
    Chapter (
        Id              VARCHAR(10)     PRIMARY KEY,

        Title           VARCHAR(56)     NOT NULL,

        PreText         VARCHAR(56)     NOT NULL,
        MainText        VARCHAR(56)     NOT NULL,
        PostText        VARCHAR(56)     NOT NULL,

        WordCount       INTEGER         NOT NULL,

        Created         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP,
        Updated         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP
    );

CREATE TABLE
IF NOT EXISTS
    Review (
        Id              VARCHAR(10)     PRIMARY KEY,

        StoryId         VARCHAR(10)     NOT NULL
                                        REFERENCES Story (Id),
        UserId          VARCHAR(10)     NOT NULL
                                        REFERENCES User (Id),
        ParentId        VARCHAR(10)     REFERENCES Review (Id),

        Content         VARCHAR(56)     NOT NULL,

        Created         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP,
        Updated         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP
    );

CREATE TABLE
IF NOT EXISTS
    StoryChapter (
        StoryId         VARCHAR(10)     NOT NULL
                                        REFERENCES Story (Id),
        ChapterId       VARCHAR(10)     NOT NULL
                                        REFERENCES Chapter (Id),

        Created         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP,
        Updated         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP,

        PRIMARY KEY (StoryId, ChapterId)
    );

CREATE TABLE
IF NOT EXISTS
    StoryAuthor (
        StoryId         VARCHAR(10)     NOT NULL
                                        REFERENCES Story (Id),
        UserId          VARCHAR(10)     NOT NULL
                                        REFERENCES User (Id),

        Created         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP,
        Updated         TIMESTAMP       NOT NULL
                                        DEFAULT CURRENT_TIMESTAMP,

        PRIMARY KEY (StoryId, UserId)
    );